# Используйте официальный образ Nginx
FROM nginx:latest

# Копируйте конфигурацию Nginx
COPY nginx.conf /etc/nginx/nginx.conf

# Копируйте статические файлы в контейнер
COPY . /usr/share/nginx/html
