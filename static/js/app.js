const todoList = document.getElementById("todo-list");
const taskInput = document.getElementById("task");

// Массив для хранения задач
const todos = [];

// Функция для добавления задачи
function addTodo() {
    const taskText = taskInput.value.trim();
    if (taskText === "") return;

    const todo = { text: taskText };
    console.log('==>', JSON.stringify(todo))
    // Отправляем POST-запрос на сервер Go
    fetch("/tasks", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(todo),
    })
        .then((response) => response.json())
        .then((newTodo) => {
            // Очищаем поле ввода
            taskInput.value = "";

            // Обновляем задачи на клиентской стороне
            todos.push(newTodo);
            renderTodos();
        })
        .catch((error) => {
            console.error("Ошибка при отправке POST-запроса:", error);
        });
}


// Функция для отображения задач
function renderTodos() {
    todoList.innerHTML = "";

    todos.forEach((todo) => {
        const li = document.createElement("li");
        li.innerHTML = `
            <span>${todo.text}</span>
            <button class="delete-button" onclick="deleteTodo(${todo.id})">Удалить</button>
        `;
        todoList.appendChild(li);
    });
}

function getAllTodos() {
    fetch("/tasks")
        .then((response) => response.json())
        .then((data) => {
            // Обновляем массив todos с полученными задачами
            todos.length = 0; // Очищаем массив
            todos.push(...data); // Добавляем полученные задачи в массив
            // Обновляем задачи на клиентской стороне
            renderTodos();
        })
        .catch((error) => {
            console.error("Ошибка при получении задач:", error);
        });
}


// Функция для удаления задачи
function deleteTodo(id) {
    const index = todos.findIndex((todo) => todo.id === id);
    if (index !== -1) {
        todos.splice(index, 1);
        renderTodos();
    }
}

// Инициализация при загрузке страницы
renderTodos();

